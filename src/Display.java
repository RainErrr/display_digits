import java.util.Map;
import java.util.stream.IntStream;
import java.util.stream.Stream;

public class Display {
    String printLCD(int digits) {
        String[] digitsToArray = Integer.toString(digits).split("");
        Map<String, String[]> template = Map.of(
                "1", new String[]{"    ", "   |", "   |", "   |", "   |"},
                "2", new String[]{" __ ", "   |", " __|", "|   ", "|__ "},
                "3", new String[]{" __ ", "   |", " __|", "   |", " __|"},
                "4", new String[]{"    ", "|  |", "|__|", "   |", "   |"},
                "5", new String[]{" __ ", "|   ", "|__ ", "   |", " __|"},
                "6", new String[]{" __ ", "|   ", "|__ ", "|  |", "|__|"},
                "7", new String[]{" __ ", "   |", "   |", "   |", "   |"},
                "8", new String[]{" __ ", "|  |", "|__|", "|  |", "|__|"},
                "9", new String[]{" __ ", "|  |", "|__|", "   |", " __|"},
                "0", new String[]{" __ ", "|  |", "|  |", "|  |", "|__|"}
        );

        String[] resultArray = {"", "", "", "", ""};

        IntStream.range(0, digitsToArray.length)
                .forEach(i -> IntStream.range(0, resultArray.length)
                        .forEach(j -> resultArray[j] += template.get(digitsToArray[i])[j]));

        return Stream.of(resultArray).reduce("", (result, row) -> result += row + "\n");
    }

    public static void main(String[] args) {
        Display display = new Display();
        System.out.println(display.printLCD(78954321));
    }
}
