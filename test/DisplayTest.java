import org.junit.jupiter.api.Test;

import static org.junit.jupiter.api.Assertions.*;

class DisplayTest {
    Display display = new Display();

    @Test
    public void printLCD_print_1 () {
        String expected =
                "    \n" +
                "   |\n" +
                "   |\n" +
                "   |\n" +
                "   |\n";
        assertEquals(expected, display.printLCD(1));
    }

    @Test
    public void printLCD_print_2 () {
        String expected =
                " __ \n" +
                "   |\n" +
                " __|\n" +
                "|   \n" +
                "|__ \n";
        assertEquals(expected, display.printLCD(2));
    }

    @Test
    public void printLCD_print_3 () {
        String expected =
                " __ \n" +
                "   |\n" +
                " __|\n" +
                "   |\n" +
                " __|\n";
        assertEquals(expected, display.printLCD(3));
    }

    @Test
    public void printLCD_print_4 () {
        String expected =
                "    \n" +
                "|  |\n" +
                "|__|\n" +
                "   |\n" +
                "   |\n";
        assertEquals(expected, display.printLCD(4));
    }

    @Test
    public void printLCD_print_5 () {
        String expected =
                " __ \n" +
                "|   \n" +
                "|__ \n" +
                "   |\n" +
                " __|\n";
        assertEquals(expected, display.printLCD(5));
    }

    @Test
    public void printLCD_print_6 () {
        String expected =
                " __ \n" +
                "|   \n" +
                "|__ \n" +
                "|  |\n" +
                "|__|\n";
        assertEquals(expected, display.printLCD(6));
    }

    @Test
    public void printLCD_print_7 () {
        String expected =
                " __ \n" +
                "   |\n" +
                "   |\n" +
                "   |\n" +
                "   |\n";
        assertEquals(expected, display.printLCD(7));
    }

    @Test
    public void printLCD_print_8 () {
        String expected =
                " __ \n" +
                "|  |\n" +
                "|__|\n" +
                "|  |\n" +
                "|__|\n";
        assertEquals(expected, display.printLCD(8));
    }

    @Test
    public void printLCD_print_9 () {
        String expected =
                " __ \n" +
                "|  |\n" +
                "|__|\n" +
                "   |\n" +
                " __|\n";
        assertEquals(expected, display.printLCD(9));
    }

    @Test
    public void printLCD_print_0 () {
        String expected =
                " __ \n" +
                "|  |\n" +
                "|  |\n" +
                "|  |\n" +
                "|__|\n";
        assertEquals(expected, display.printLCD(0));
    }

    @Test
    public void printLCD_print_10 () {
        String expected =
                "     __ \n" +
                "   ||  |\n" +
                "   ||  |\n" +
                "   ||  |\n" +
                "   ||__|\n";
        assertEquals(expected, display.printLCD(10));
    }

    @Test
    public void printLCD_print_123 () {
        String expected =
                "     __  __ \n" +
                "   |   |   |\n" +
                "   | __| __|\n" +
                "   ||      |\n" +
                "   ||__  __|\n";
        assertEquals(expected, display.printLCD(123));
    }

    @Test
    public void printLCD_print_987654321 () {
        String expected =
                " __  __  __  __  __      __  __     \n" +
                "|  ||  |   ||   |   |  |   |   |   |\n" +
                "|__||__|   ||__ |__ |__| __| __|   |\n" +
                "   ||  |   ||  |   |   |   ||      |\n" +
                " __||__|   ||__| __|   | __||__    |\n";
        assertEquals(expected, display.printLCD(987654321));
    }
}